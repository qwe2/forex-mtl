import Dependencies._

name := "forex"
version := "1.0.0"

scalaVersion := "2.12.8"
scalacOptions ++= Seq(
 "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:experimental.macros",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xfuture",
  "-Xlint",
  "-Ydelambdafy:method",
  "-Xlog-reflective-calls",
  "-Yno-adapted-args",
  "-Ypartial-unification",
  "-Ywarn-dead-code",
  "-Ywarn-inaccessible",
  "-Ywarn-unused-import",
  "-Ywarn-value-discard"
)

resolvers +=
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

lazy val root = (project in file("."))
  .configs(IntegrationTest.extend(Test))
  .settings(
    Defaults.itSettings,
    exportJars in Test := false,
    exportJars in IntegrationTest := false,
    libraryDependencies ++= Seq(
      compilerPlugin(Libraries.kindProjector),
      Libraries.cats,
      Libraries.catsEffect,
      Libraries.alleyCats,
      Libraries.fs2,
      Libraries.http4sDsl,
      Libraries.http4sServer,
      Libraries.http4sCirce,
      Libraries.http4sClient,
      Libraries.circeCore,
      Libraries.circeGeneric,
      Libraries.circeGenericExt,
      Libraries.circeParser,
      Libraries.circeJava8,
      Libraries.pureConfig,
      Libraries.shapeless,
      Libraries.logback,
      Libraries.scalaLog,
      Libraries.scalaTest        % Test,
      Libraries.scalaCheck       % Test,
      Libraries.catsScalaCheck   % Test
    )
  )
