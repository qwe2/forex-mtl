package forex.utils

import cats.effect.Timer
import fs2.Stream

import scala.concurrent.duration.FiniteDuration

object StreamUtils {
  def retryUntil[F[_]: Timer, T](
      f: F[T],
      isDone: T => Boolean,
      maxAttempts: Long,
      initialDelay: FiniteDuration,
      nextDelay: FiniteDuration => FiniteDuration
  ): Stream[F, T] = {
    val delayStream = Stream.unfold(initialDelay)(d => Some(d -> nextDelay(d))).covary[F]

    (Stream.eval(f) ++ delayStream.flatMap(delay => Stream.sleep_(delay) ++ Stream.eval(f)))
      .take(maxAttempts)
      .takeThrough(x => !isDone(x))
      .last
      .map(_.get)
  }
}
