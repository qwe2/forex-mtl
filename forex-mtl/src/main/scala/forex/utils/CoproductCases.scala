package forex.utils

import shapeless.{ :+:, CNil, Coproduct, Generic, Witness }

trait CoproductCases[T] {
  def nodes: List[T]
}

object CoproductCases {
  trait Aux[T, Repr] {
    def nodes: List[T]
  }

  implicit def cNil[T]: Aux[T, CNil] = new Aux[T, CNil] {
    override def nodes: List[T] = Nil
  }
  implicit def cCons[T, Head <: T, Tail <: Coproduct](
      implicit
      h: Witness.Aux[Head],
      t: Aux[T, Tail]
  ): Aux[T, Head :+: Tail] =
    new Aux[T, Head :+: Tail] {
      override def nodes: List[T] = h.value :: t.nodes
    }

  implicit def generic[T, C <: Coproduct](
      implicit
      gen: Generic.Aux[T, C],
      cc: Aux[T, C]
  ): CoproductCases[T] = new CoproductCases[T] {
    override def nodes: List[T] = {
      // convince scalac that gen is used
      val _ = gen
      cc.nodes
    }
  }

  def apply[T](implicit cc: CoproductCases[T]): List[T] = cc.nodes
}
