package forex

package object utils {
  private[utils] def trimCaseObject(name: String): String = {
    if (name.endsWith(".type")) name.substring(0, name.length - ".type".length)
    else name
  }
}
