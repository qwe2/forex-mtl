package forex.utils

import cats.Show
import shapeless.{ :+:, CNil, Coproduct, Generic, Typeable }

object CoproductShow {
  implicit val showCNil: Show[CNil] =
    Show.show(_ => throw new RuntimeException("Can't happen"))

  implicit def showCCons[Head, Tail <: Coproduct](
      implicit
      h: Typeable[Head],
      t: Show[Tail]
  ): Show[Head :+: Tail] =
    Show.show(_.eliminate(_ => trimCaseObject(h.describe), t.show))

  implicit def showGeneric[T, C <: Coproduct](
      implicit
      g: Generic.Aux[T, C],
      c: Show[C]
  ): Show[T] = Show.show(g.to _ andThen c.show)
}
