package forex.utils

import java.time.{Instant, OffsetDateTime, ZoneOffset}
import java.util.concurrent.TimeUnit

import cats.Functor
import cats.effect.Clock
import cats.syntax.functor._

object JavaTime {
  def offsetDateTimeNow[F[_]: Clock: Functor]: F[OffsetDateTime] =
    Clock[F].realTime(TimeUnit.MILLISECONDS).map { time =>
      val instant = Instant.ofEpochMilli(time)
      OffsetDateTime.ofInstant(instant, ZoneOffset.UTC)
    }
}
