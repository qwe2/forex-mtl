package forex.config

import scala.concurrent.duration.FiniteDuration

case class ApplicationConfig(
    http: HttpConfig,
    services: Services
)

case class HttpConfig(
    host: String,
    port: Int,
    timeout: FiniteDuration
)

case class Services(oneForge: OneForge)

case class OneForge(
    baseUrl: String,
    apiKey: String,
    cacheRetentionTime: FiniteDuration
)
