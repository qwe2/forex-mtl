package forex

import cats.effect.{ Concurrent, ConcurrentEffect, Timer }
import cats.syntax.functor._
import forex.config.ApplicationConfig
import forex.http.rates.RatesHttpRoutes
import forex.programs._
import forex.services._
import org.http4s._
import org.http4s.client.Client
import org.http4s.implicits._
import org.http4s.server.middleware.{ AutoSlash, Timeout }

class Module[F[_]: Concurrent: Timer](ratesProgram: RatesProgram[F], config: ApplicationConfig) {

  private val ratesHttpRoutes: HttpRoutes[F] = new RatesHttpRoutes[F](ratesProgram, config).routes

  type PartialMiddleware = HttpRoutes[F] => HttpRoutes[F]
  type TotalMiddleware   = HttpApp[F] => HttpApp[F]

  private val routesMiddleware: PartialMiddleware = {
    { http: HttpRoutes[F] =>
      AutoSlash(http)
    }
  }

  private val appMiddleware: TotalMiddleware = { http: HttpApp[F] =>
    Timeout(config.http.timeout)(http)
  }

  private val http: HttpRoutes[F] = ratesHttpRoutes

  val httpApp: HttpApp[F] = appMiddleware(routesMiddleware(http).orNotFound)

}

object Module {
  def apply[F[_]: Concurrent: Timer: ConcurrentEffect](client: Client[F], config: ApplicationConfig): F[Module[F]] =
    RatesServices.liveCached(client, config.services.oneForge).map { ratesService =>
      new Module(RatesProgram[F](ratesService), config)
    }
}
