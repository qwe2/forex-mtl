package forex.services.rates

import cats.Functor
import cats.syntax.functor._
import forex.domain.Rate
import forex.services.rates.errors.Error.OneForgeLookupFailed
import forex.services.rates.errors._

trait Algebra[F[_]] {
  protected def F: Functor[F]
  private implicit lazy val FF: Functor[F] = F

  def get(pair: Rate.Pair): F[Error Either Rate] = {
    get(Set(pair)).map(_.flatMap(_.headOption.toRight(OneForgeLookupFailed("Empty response"))))
  }

  def get(pair: Set[Rate.Pair]): F[Error Either List[Rate]]
}
