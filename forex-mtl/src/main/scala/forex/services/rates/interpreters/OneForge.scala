package forex.services.rates.interpreters

import java.time.ZoneOffset

import cats.Functor
import cats.data.EitherT
import cats.effect.Sync
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import forex.config
import forex.domain.Currency.show
import forex.domain.{ Currency, Price, Rate, Timestamp }
import forex.http._
import forex.services.rates.{ errors, Algebra }
import org.http4s.client.Client
import org.http4s.{ Method, Request, Uri, _ }

import scala.collection.mutable

class OneForge[F[_]: Sync](
    client: Client[F],
    conf: config.OneForge
) extends Algebra[F]
    with LazyLogging {
  import OneForge._

  override protected val F: Functor[F] = implicitly[Functor[F]]

  override def get(requests: Set[Rate.Pair]): F[Either[errors.Error, List[Rate]]] =
    EitherT(
      Sync[F]
        .delay(logger.debug(s"Sending OneForge requests: $requests"))
        .map(_ => makeQuotesRequestString(conf.baseUrl, conf.apiKey, requests.map(r => r.from -> r.to).toList))
    ).flatMap(doRequest)
      .map(_.toList)
      .nested
      .map(_.toRate)
      .value
      .value

  private def doRequest(req: Request[F]): EitherT[F, errors.Error, mutable.WrappedArray[OneForgeOkResponse]] = {
    val response =
      client
        .expect[OneForgeResponse](req)
        .flatTap(response => Sync[F].delay(logger.debug(s"Got OneForge response: $response")))
        .map(_.leftMap(_.toApiError))
        .onError {
          case t: Throwable =>
            Sync[F].delay(logger.error("Error requesting rate from OneForge", t))
        }
    EitherT(response).leftWiden[errors.Error]
  }

  private def makeQuotesRequestString(baseUrl: String, apiKey: String, currencyPairs: List[(Currency, Currency)]) = {
    val base          = Uri.fromString(baseUrl)
    val currencyParam = currencyPairs.map { case (c1, c2) => show(c1) + show(c2) }.mkString(",")
    base
      .map(
        _.resolve(uri"quotes")
          .withQueryParam("pairs", currencyParam)
          .withQueryParam("api_key", apiKey)
      )
      .map(uri => Request[F](method = Method.GET, uri = uri))
      .leftMap(parseError => errors.Error.OneForgeLookupFailed(parseError.message))
      .leftWiden[errors.Error]
  }
}

object OneForge {
  import io.circe._
  import io.circe.generic.semiauto._

  def apply[F[_]: Sync](client: Client[F], conf: config.OneForge): OneForge[F] =
    new OneForge[F](client, conf)

  private type OneForgeResponse = Either[OneForgeError, mutable.WrappedArray[OneForgeOkResponse]]

  private final case class OneForgeOkResponse(
      symbol: FromToSymbol,
      price: BigDecimal,
      bid: BigDecimal,
      ask: BigDecimal,
      timestamp: Timestamp
  ) {
    def toRate: Rate = Rate(Rate.Pair(symbol.from, symbol.to), Price(price), timestamp)
  }

  private final case class OneForgeError(message: String) {
    def toApiError: errors.Error = errors.Error.OneForgeLookupFailed(message)
  }

  private final case class FromToSymbol(from: Currency, to: Currency)

  private implicit val timestampDecoder: Decoder[Timestamp] =
    Decoder.instance { cursor =>
      cursor.as[Long].map(Timestamp.fromEpochSeconds(_, ZoneOffset.UTC))
    }

  private implicit val fromToSymbolDecoder: Decoder[FromToSymbol] = Decoder.instance { cursor =>
    cursor.as[String].flatMap { fromTo =>
      def error(s: String)      = DecodingFailure(s"OneForge API response error: $s", cursor.history)
      def unknownSym(s: String) = error(s"$s was an unknown Currency symbol")

      Either
        .cond(
          fromTo.length == 6,
          fromTo,
          error("symbol has to be the concatenation of 2 symbols")
        )
        .map(_.splitAt(3))
        .flatMap {
          case (from, to) =>
            for {
              f <- Either.fromOption(Currency.fromString.lift(from), unknownSym("from"))
              t <- Either.fromOption(Currency.fromString.lift(to), unknownSym("to"))
            } yield FromToSymbol(f, t)
        }
    }
  }

  private implicit val oneForgeErrorDecoder: Decoder[OneForgeError] =
    deriveDecoder[OneForgeError]

  private implicit val oneForgeResponseDecoder: Decoder[OneForgeOkResponse] = {
    // convince scalac that these are used
    val (_, _) = (fromToSymbolDecoder, timestampDecoder)
    deriveDecoder[OneForgeOkResponse]
  }

  private implicit val oneForgeResponseDec: Decoder[OneForgeResponse] = Decoder.instance { cursor =>
    val isError = cursor.get[Boolean]("error").getOrElse(false)
    if (!isError) cursor.as[mutable.WrappedArray[OneForgeOkResponse]].map(Either.right)
    else cursor.as[OneForgeError].map(Either.left)
  }
}
