package forex.services.rates.interpreters

import alleycats.std.set._
import cats.data.EitherT
import cats.effect.concurrent.Semaphore
import cats.effect.{ Concurrent, Sync, Timer }
import cats.implicits._
import cats.{ Applicative, Functor }
import com.typesafe.scalalogging.LazyLogging
import forex.domain.Rate
import forex.services.cache.{ Cache, RefreshPolicy }
import forex.services.rates.errors.Error.CacheRefreshTimeout
import forex.services.rates.{ errors, Algebra }
import forex.utils.StreamUtils._

import scala.concurrent.duration.{ FiniteDuration, _ }

class Cached[F[_]: Sync: Concurrent: Timer: Applicative] private (
    live: Algebra[F],
    retentionPolicy: Rate => F[Boolean],
    rateCache: Cache[F, Rate.Pair, Rate],
    refreshPolicy: RefreshPolicy[F, Rate.Pair],
    refreshLock: Semaphore[F]
) extends Algebra[F]
    with LazyLogging {

  override protected val F: Functor[F] = implicitly[Functor[F]]

  private[this] val S = implicitly[Sync[EitherT[F, errors.Error, ?]]]

  override def get(pair: Set[Rate.Pair]): F[Either[errors.Error, List[Rate]]] =
    getOrQuery(pair).value.flatTap(response => Sync[F].delay(logger.debug(s"Cache is returning $response")))

  private def putAll(values: Set[Rate]): F[Unit] =
    rateCache.putAll(values.map(rate => rate.pair -> rate).toMap) >>
      Sync[F].delay(logger.debug(s"Put all of $values into the cache"))

  private def shouldRetain(rate: Rate): F[Boolean] =
    retentionPolicy(rate).flatTap(
      retained => if (!retained) Sync[F].delay(logger.debug(s"Rate $rate has expired")) else Sync[F].unit
    )

  private def getFromCache(currencyPairs: Set[Rate.Pair]): F[Map[Rate.Pair, Option[Rate]]] =
    rateCache
      .getAll(currencyPairs)
      .map(_.toSet)
      .flatMap(
        _.traverse {
          case (key, value) =>
            Sync[F].delay(logger.debug(s"Got $value from the cache for $key")) >>
              value.filterA(shouldRetain).map(key -> _)
        }
      )
      .map(_.toMap)

  private def refreshItems(items: Set[Rate.Pair], releaseResource: F[Unit]): EitherT[F, errors.Error, List[Rate]] =
    S.delay(logger.debug(s"Refreshing rates: $items"))
      .flatMapF(_ => live.get(items))
      .semiflatMap { liveRates =>
        Concurrent[F].start(putAll(liveRates.toSet).attempt.flatTap(_ => releaseResource).rethrow) >>
          liveRates.pure[F]
      }

  private def getOrQuery(currencyPairs: Set[Rate.Pair]): EitherT[F, errors.Error, List[Rate]] = {
    val attempts = 7
    // times out after ~1200ms
    val retryingOptimisticRefresh = retryUntil(
      attemptGetOrQuery(currencyPairs).value,
      (result: Either[errors.Error, Option[List[Rate]]]) => result.fold(_ => true, _.isDefined),
      attempts,
      50.milliseconds,
      (dur: FiniteDuration) => (dur.toNanos * 1.4).nanos
    ).compile.last.map(_.get)

    EitherT(retryingOptimisticRefresh).flatMap(
      maybeRates =>
        EitherT.fromEither(
          maybeRates.toRight(CacheRefreshTimeout(s"Failed to refresh the cache after $attempts tries"))
      )
    )
  }

  /**
    * Attempts to get the element from the cache. Tries to refresh it if it's missing.
    * @param currencyPairs
    * @return Right(Some(list)) if the element was in the cache or a successful cache refresh returned it,
    *         Right(None) if the element wasn't in the cache but there's already a concurrent refresh
    *         Left(error) if the element wasn't in the cache and the refresh failed for some reason
    */
  private def attemptGetOrQuery(currencyPairs: Set[Rate.Pair]): EitherT[F, errors.Error, Option[List[Rate]]] =
    EitherT.liftF(getFromCache(currencyPairs)).flatMap { cached =>
      val (found, notFound) = cached.partition(_._2.isDefined).bimap(_.values.map(_.get), _.keySet)

      S.delay(logger.debug(s"Found in cache: $found, not found: $notFound")) >>
        (
          if (notFound.isEmpty) EitherT.pure(Some(found.toList))
          else {
            EitherT
              .liftF(refreshLock.tryAcquire)
              .flatMap {
                case false => EitherT.pure(None)
                case true =>
                  EitherT
                    .liftF(refreshPolicy.getItemsToRefresh(notFound))
                    .flatMap(items => refreshItems(items, refreshLock.release))
                    .nested
                    .filter(rate => currencyPairs(rate.pair))
                    .value
                    .map(Some(_))
              }
          }
        )
    }
}

object Cached {
  def apply[F[_]: Sync: Concurrent: Timer](
      live: Algebra[F],
      retentionPolicy: Rate => F[Boolean],
      rateCache: Cache[F, Rate.Pair, Rate],
      refreshPolicy: RefreshPolicy[F, Rate.Pair]
  ): F[Cached[F]] =
    Semaphore[F](1).map(semaphore => new Cached(live, retentionPolicy, rateCache, refreshPolicy, semaphore))
}
