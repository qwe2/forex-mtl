package forex.services.rates

import java.time.OffsetDateTime

import cats.Functor
import cats.effect.{ Clock, ConcurrentEffect, Sync, Timer }
import cats.syntax.flatMap._
import cats.syntax.functor._
import forex.config
import forex.domain.{ Currency, Rate }
import forex.services.cache.{ InMemoryCache, RefreshPolicy }
import forex.services.rates.interpreters._
import forex.utils.JavaTime
import org.http4s.client.Client

import scala.concurrent.duration.FiniteDuration

object Interpreters {
  def liveCached[F[_]: Sync: ConcurrentEffect: Timer: Clock](client: Client[F], conf: config.OneForge): F[Algebra[F]] =
    for {
      live <- Sync[F].pure(OneForge[F](client, conf))
      currencyCount = Currency.values.size
      cacheSize     = currencyCount * (currencyCount - 1)
      cache <- InMemoryCache[F, Rate.Pair, Rate](takeNewerRate, cacheSize)
      refreshPolicy = RefreshPolicy.oneForge
      cached <- Cached[F](live, retentionPolicyFromDuration[F](conf.cacheRetentionTime), cache, refreshPolicy)
    } yield cached

  private def retentionPolicyFromDuration[F[_]: Clock: Functor](dur: FiniteDuration)(rate: Rate): F[Boolean] =
    JavaTime.offsetDateTimeNow.map { now =>
      val rateTs = rate.timestamp.value
      now.minusNanos(dur.toNanos).compareTo(rateTs) <= 0
    }

  private def takeNewerRate(oldRate: Rate, newRate: Rate): Rate =
    Ordering.by[Rate, OffsetDateTime](_.timestamp.value).max(oldRate, newRate)
}
