package forex.services.cache

import cats.Applicative
import forex.domain.{Currency, Rate}

trait RefreshPolicy[F[_], T] {
  def getItemsToRefresh(staleItems: Set[T]): F[Set[T]]
}

object RefreshPolicy {
  // OneForge counts querying multiple pairs in the same request as one query towards the quota
  def oneForge[F[_]: Applicative]: RefreshPolicy[F, Rate.Pair] = new RefreshPolicy[F, Rate.Pair] {
    private[this] val allRatePairs =
      (for { from <- Currency.values; to <- Currency.values if from != to } yield Rate.Pair(from, to)).toSet

    override def getItemsToRefresh(staleItems: Set[Rate.Pair]): F[Set[Rate.Pair]] =
      Applicative[F].pure(allRatePairs)
  }
}
