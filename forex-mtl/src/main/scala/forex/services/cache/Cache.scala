package forex.services.cache

import alleycats.std.set._
import cats.Applicative
import cats.syntax.foldable._
import cats.syntax.traverse._
import cats.syntax.functor._

trait Cache[F[_], K, V] {
  protected def A: Applicative[F]
  private implicit lazy val AA: Applicative[F] = A

  def get(key: K): F[Option[V]]
  def getAll(keys: Set[K]): F[Map[K, Option[V]]] =
    keys.traverse(key => get(key).map(key -> _)).map(_.toMap)

  def put(key: K, value: V): F[Unit]
  def putAll(keys: Map[K, V]): F[Unit] =
    keys.toSet.traverse_ { case (key, value) => put(key, value) }
}
