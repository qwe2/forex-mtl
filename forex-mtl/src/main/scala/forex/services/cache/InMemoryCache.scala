package forex.services.cache

import java.util.concurrent.ConcurrentHashMap

import cats.Applicative
import cats.effect.Sync
import cats.syntax.functor._

class InMemoryCache[F[_]: Sync, K, V] private (
    backing: ConcurrentHashMap[K, V],
    mergeFunction: (V, V) => V
) extends Cache[F, K, V] {
  override protected def A: Applicative[F] = implicitly[Applicative[F]]

  override def get(key: K): F[Option[V]] =
    Sync[F].delay(backing.get(key)).map(Option.apply)

  override def put(key: K, value: V): F[Unit] =
    Sync[F].delay(backing.merge(key, value, (v1, v2) => mergeFunction(v1, v2))).void
}

object InMemoryCache {
  def apply[F[_]: Sync, K, V](
      mergeFunction: (V, V) => V,
      initialCapacity: Int = 16,
      loadFactor: Float = 0.75.toFloat
  ): F[Cache[F, K, V]] =
    Sync[F]
      .delay(new ConcurrentHashMap[K, V](initialCapacity, loadFactor))
      .map(map => new InMemoryCache(map, mergeFunction))
}
