package forex

import cats.effect._
import cats.syntax.functor._
import com.typesafe.scalalogging.LazyLogging
import forex.config._
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext

object Main extends IOApp with LazyLogging {

  private[this] val executionContext =
    ExecutionContext.fromExecutorService(null, t => logger.error("Unhandled exception: ", t))

  override protected implicit def contextShift: ContextShift[IO] =
    IO.contextShift(executionContext)

  override protected implicit def timer: Timer[IO] =
    IO.timer(executionContext)

  override def run(args: List[String]): IO[ExitCode] =
    new Application[IO](executionContext).stream.compile.drain.as(ExitCode.Success)

}

class Application[F[_]: ConcurrentEffect: Timer](ec: ExecutionContext) {

  def stream: Stream[F, Unit] =
    for {
      config <- Config.stream("app")
      client <- BlazeClientBuilder[F](ec).stream
      module <- Stream.eval(Module[F](client, config))
      _ <- BlazeServerBuilder[F]
            .bindHttp(config.http.port, config.http.host)
            .withHttpApp(module.httpApp)
            .serve
    } yield ()

}
