package forex.domain

import cats.Show
import forex.utils.CoproductCases

sealed trait Currency

object Currency {
  import CoproductCases._
  import forex.utils.CoproductShow._

  case object AUD extends Currency
  case object CAD extends Currency
  case object CHF extends Currency
  case object EUR extends Currency
  case object GBP extends Currency
  case object NZD extends Currency
  case object JPY extends Currency
  case object SGD extends Currency
  case object USD extends Currency

  def show(c: Currency): String     = Show[Currency].show(c)
  lazy val values: List[Currency]   = CoproductCases[Currency]
  lazy val valueNames: List[String] = values.map(show)

  val fromString: PartialFunction[String, Currency] = {
    case "AUD" => AUD
    case "CAD" => CAD
    case "CHF" => CHF
    case "EUR" => EUR
    case "GBP" => GBP
    case "NZD" => NZD
    case "JPY" => JPY
    case "SGD" => SGD
    case "USD" => USD
  }

}
