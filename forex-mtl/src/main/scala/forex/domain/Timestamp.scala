package forex.domain

import java.time.{ Instant, OffsetDateTime, ZoneId }

import cats.Functor
import cats.effect.Clock
import cats.syntax.functor._
import forex.utils.JavaTime

case class Timestamp(value: OffsetDateTime) extends AnyVal

object Timestamp {
  def now[F[_]: Clock: Functor]: F[Timestamp] =
    JavaTime.offsetDateTimeNow[F].map(Timestamp.apply)

  def fromEpochSeconds(epochSeconds: Long, zoneId: ZoneId): Timestamp = {
    val instant = Instant.ofEpochSecond(epochSeconds)
    Timestamp(OffsetDateTime.ofInstant(instant, zoneId))
  }

}
