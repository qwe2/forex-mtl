package forex.domain

case class Price(value: BigDecimal) extends AnyVal

object Price {
  def int(value: Integer): Price =
    Price(BigDecimal(value))
}
