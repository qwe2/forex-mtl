package forex.http
package rates

import java.time.{ OffsetDateTime, Duration => JDuration }

import cats.effect.{ Clock, Sync }
import cats.implicits._
import forex.config.ApplicationConfig
import forex.http.rates.errors.ServiceError
import forex.http.rates.errors.ServiceError.InvalidQueryParams
import forex.programs.RatesProgram
import forex.programs.rates.{ Protocol => RatesProgramProtocol }
import forex.utils.JavaTime
import org.http4s.CacheDirective.`max-age`
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Cache-Control`
import org.http4s.server.Router

import scala.concurrent.duration._

class RatesHttpRoutes[F[_]: Sync: Clock](rates: RatesProgram[F], config: ApplicationConfig) extends Http4sDsl[F] {

  import Converters._
  import Protocol._
  import QueryParams._

  private[http] val prefixPath = "/rates"

  private def cacheExpirationDateSeconds(timestamp: OffsetDateTime): F[Duration] =
    JavaTime.offsetDateTimeNow
      .map(
        now =>
          JDuration
            .between(now, timestamp.plusNanos(config.services.oneForge.cacheRetentionTime.toNanos))
            .getSeconds
            .seconds
      )

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root :? FromQueryParam(from) +& ToQueryParam(to) =>
      (from, to)
        .mapN { (from, to) =>
          rates.get(RatesProgramProtocol.GetRatesRequest(from, to)).flatMap {
            case Left(error) =>
              Ok(error.asGetApiError)
            case Right(rate) =>
              for {
                expiresInSeconds <- cacheExpirationDateSeconds(rate.timestamp.value)
                response <- Ok(rate.asGetApiResponse)
              } yield response.putHeaders(`Cache-Control`(`max-age`(expiresInSeconds)))
          }
        }
        .valueOr(
          failure =>
            BadRequest(
              ServiceError(
                InvalidQueryParams(failure.toList.map(_.sanitized))
              )
          )
        )
  }

  val routes: HttpRoutes[F] = Router(
    prefixPath -> httpRoutes
  )

}
