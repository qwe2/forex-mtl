package forex.http.rates

object errors {
  final case class ServiceError(error: ServiceErrorType)
  sealed trait ServiceErrorType extends Product with Serializable

  object ServiceError {
    import io.circe._
    import io.circe.generic.extras.semiauto._
    import io.circe.generic.extras.Configuration

    final case class InvalidQueryParams(errors: List[String]) extends ServiceErrorType

    implicit val errorConfig: Configuration =
      Configuration.default
        .withSnakeCaseMemberNames
        .withSnakeCaseConstructorNames

    implicit val errorTypeEncoder: Encoder[ServiceErrorType] = deriveEncoder
    implicit val encoder: Encoder[errors.ServiceError] = deriveEncoder
  }

}
