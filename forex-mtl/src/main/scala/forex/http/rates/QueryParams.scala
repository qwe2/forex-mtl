package forex.http.rates

import cats.data.Validated
import forex.domain.Currency
import org.http4s.dsl.impl.ValidatingQueryParamDecoderMatcher
import org.http4s.{ParseFailure, QueryParamDecoder, QueryParameterValue}

object QueryParams {

  private[http] def currencyQueryParam(key: String): QueryParamDecoder[Currency] =
    (value: QueryParameterValue) => {
      Validated
        .fromOption(
          Currency.fromString.lift(value.value),
          ParseFailure(s"""param "$key" has to be a valid three character currency symbol, """ +
            s"valid symbols are: ${Currency.values.mkString(", ")}", value.value)
        )
        .toValidatedNel
    }

  object FromQueryParam extends ValidatingQueryParamDecoderMatcher[Currency]("from")(
    currencyQueryParam("from"))
  object ToQueryParam extends ValidatingQueryParamDecoderMatcher[Currency]("to")(
    currencyQueryParam("to")
  )

}
