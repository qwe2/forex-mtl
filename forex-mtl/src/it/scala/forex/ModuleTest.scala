package forex

import java.time.OffsetDateTime
import java.util.concurrent.TimeUnit

import cats.effect._
import forex.config.ApplicationConfig
import forex.domain.Currency
import forex.domain.Currency.{ AUD, USD }
import io.circe.{ Decoder, HCursor, Json }
import org.http4s.client.Client
import org.http4s.{ Method, Request, Response, Status }
import org.scalatest.WordSpec
import org.http4s._
import cats.syntax.parallel._
import cats.instances.list._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class ModuleTest extends WordSpec {
  import ModuleTest._
  import org.http4s.circe.CirceEntityDecoder._

  "Module" should {
    def req(from: Currency, to: Currency) = {
      val f = Currency.show(from)
      val t = Currency.show(to)
      Request[IO](method = Method.GET, uri = Uri.fromString(s"/rates/?from=$f&to=$t").right.get)
    }
    val baseRequest = req(USD, AUD)

    "parse request and return a response" in {
      val module = createModule(mockClient()).unsafeRunSync()

      val response = module.httpApp.run(baseRequest).unsafeRunSync()
      val jsResp   = response.as[Json].unsafeRunSync().hcursor
      assert(unsafeGetField[String](jsResp, "from") === "USD")
      assert(unsafeGetField[String](jsResp, "to") === "AUD")
      assert(unsafeGetField[Double](jsResp, "price") >= 0)
      assert(unsafeGetField[OffsetDateTime](jsResp, "timestamp") !== null)
    }

    "handle concurrent requests" in {
      val module = createModule(mockClient(100.millis)).unsafeRunSync()

      val requests = (for {
        from <- Currency.values
        to <- Currency.values if from != to
      } yield req(from, to)).take(50).map(module.httpApp.run)

      val response = requests.parSequence.unsafeRunSync()
      response.map(_.as[Json].unsafeRunSync()).foreach { js =>
        assert(unsafeGetField[Double](js.hcursor, "price") >= 0)
      }
    }

    "errors are proagated" in {
      val module = createModule(errorClient).unsafeRunSync()

      val response = module.httpApp.run(baseRequest).unsafeRunSync()
      val jsResp   = response.as[Json].unsafeRunSync()
      assert(unsafeGetField[String](jsResp.hcursor, "message") === "ERROR")
    }
  }
}

object ModuleTest {
  import cats.implicits._
  import io.circe.parser._
  import org.http4s.circe.CirceEntityEncoder._
  import pureconfig.generic.auto._

  private[this] val executionContext =
    ExecutionContext.fromExecutorService(null)

  private implicit def contextShift: ContextShift[IO] =
    IO.contextShift(executionContext)

  private implicit def timer: Timer[IO] =
    IO.timer(executionContext)

  private val queryRegex = """pairs=(.+?)&api_key=""".r

  private def errorClient = Client[IO] { _ =>
    val json = parse("""{"error": true, "message": "ERROR"}""").right.get

    Resource.make(
      Response[IO](status = Status.Accepted).withEntity(json).pure[IO]
    )(_ => IO.unit)
  }

  private def mockClient(delay: FiniteDuration = 0.seconds) =
    Client[IO] { req =>
      val query = queryRegex.findFirstMatchIn(req.queryString).get
      val pairs = query.group(1).split("%2C") // ","
      val json = pairs.toList
        .traverse { sym =>
          for {
            time <- Clock[IO].realTime(TimeUnit.SECONDS)
            price <- IO.delay(BigDecimal(Math.random()))
          } yield s"""{ "symbol": "$sym", "bid": $price, "ask": $price, "price": $price, "timestamp": $time }"""
        }
        .map(_.mkString("[", ",", "]"))
        .map(js => parse(js).right.get)

      Resource.make(
        json.flatTap(_ => IO.sleep(delay)).map(response => Response[IO](status = Status.Accepted).withEntity(response))
      )(_ => IO.unit)
    }

  private def createModule(client: Client[IO]): IO[Module[IO]] =
    for {
      config <- IO.delay(pureconfig.loadConfigOrThrow[ApplicationConfig]("app"))
      module <- Module[IO](client, config)
    } yield module

  private def unsafeGetField[T: Decoder](cursor: HCursor, key: String): T =
    cursor.get[T](key).right.get
}
