package forex.services.cache

import cats.effect.IO
import cats.syntax.flatMap._
import org.scalatest.WordSpec

class InMemoryCacheTest extends WordSpec {
  import InMemoryCacheTest._

  "InMemoryCache" should {
    "return None for missing key" in {
      val cache = mkCache
      val value = cache.get("Test").unsafeRunSync()
      assert(value === None)
    }

    "return existing item" in {
      val cache = mkCache
      val value = (cache.put("Test", 3) >> cache.get("Test")).unsafeRunSync()
      assert(value === Some(3))
    }

    "override newer item" in {
      val cache = mkCache
      val value = (cache.put("Test", 3) >> cache.put("Test", 4) >> cache.get("Test")).unsafeRunSync()
      assert(value === Some(4))
    }

    "return all items after putAll" in {
      val cache  = mkCache
      val items  = (1 to 10).map(x => x.toString -> x).toMap
      val values = (cache.putAll(items) >> cache.getAll((3 to 5).map(_.toString).toSet)).unsafeRunSync()
      assert(values === Map("3" -> Some(3), "4" -> Some(4), "5" -> Some(5)))
    }

    "return missing values in getAll" in {
      val cache  = mkCache
      val items  = (1 to 5).map(x => x.toString -> x).toMap
      val values = (cache.putAll(items) >> cache.getAll((4 to 6).map(_.toString).toSet)).unsafeRunSync()
      assert(values === Map("4" -> Some(4), "5" -> Some(5), "6" -> None))
    }
  }

}

object InMemoryCacheTest {
  private def mkCache = InMemoryCache[IO, String, Int]((_, b) => b).unsafeRunSync()
}
