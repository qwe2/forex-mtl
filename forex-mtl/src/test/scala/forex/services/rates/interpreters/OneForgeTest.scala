package forex.services.rates.interpreters

import java.time.{ LocalDateTime, ZoneOffset }

import cats.effect.{ IO, Resource }
import forex.config
import forex.domain.Currency.{ AUD, USD }
import forex.domain.{ Price, Rate }
import forex.services.rates.errors.Error.OneForgeLookupFailed
import org.http4s.client.Client
import org.http4s.{ InvalidMessageBodyFailure, Response, Status }
import org.scalatest.{ Inside, WordSpec }
import scala.concurrent.duration._

class OneForgeTest extends WordSpec with Inside {
  import OneForgeTest._

  "OneForge live interpreter" should {
    val conf = config.OneForge("NONE", "NONE", 1.hour)

    "parse valid response" in {
      val json = """ [
                   | {
                   |   "symbol": "AUDUSD",
                   |   "price": 144.3715,
                   |   "bid": 144.368,
                   |   "ask": 144.375,
                   |   "timestamp": 1502160794
                   | }
                   | ]""".stripMargin

      val of   = new OneForge[IO](clientReturning(json), conf)
      val resp = of.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      inside(resp) {
        case Right(Rate(pair, price, timestamp)) =>
          assert(pair === Rate.Pair(AUD, USD))
          assert(price === Price(BigDecimal("144.3715")))
          assert(timestamp.value === LocalDateTime.parse("2017-08-08T02:53:14").atOffset(ZoneOffset.UTC))
      }
    }

    "parse OneForge API error" in {
      val errorMsg = "API Key Not Valid."
      val json     = s"""{"error":true,"message":"$errorMsg"}"""
      val of       = new OneForge[IO](clientReturning(json), conf)
      val resp     = of.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      inside(resp) {
        case Left(OneForgeLookupFailed(error)) =>
          assert(error === errorMsg)
      }
    }

    "unexpected API response throws" in {
      val json = """["INVALID"]"""
      val of   = new OneForge[IO](clientReturning(json), conf)
      assertThrows[InvalidMessageBodyFailure] {
        of.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      }
    }
  }
}

object OneForgeTest {
  import io.circe.parser._
  import org.http4s.circe.CirceEntityEncoder._

  private def clientReturning(json: String) =
    Client[IO] { _ =>
      val response = parse(json).right.get

      Resource.make(IO.pure(Response[IO](status = Status.Accepted).withEntity(response)))(_ => IO.unit)
    }
}
