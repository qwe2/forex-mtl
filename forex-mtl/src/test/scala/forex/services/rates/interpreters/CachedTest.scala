package forex.services.rates.interpreters

import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

import cats.Functor
import cats.effect._
import forex.domain.Currency.{ AUD, CHF, USD }
import forex.domain.{ Price, Rate, Timestamp }
import forex.services.cache.{ InMemoryCache, RefreshPolicy }
import forex.services.rates.{ errors, Algebra }
import org.scalatest.WordSpec

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.TimeUnit

class CachedTest extends WordSpec {
  import CachedTest._

  "A cached rate pair" should {
    "return from cache on second query" in {
      val cached = mkCached(countingInterpreter, _ => true)
      val first  = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      val second = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()

      assert(first.isRight)
      assert(first === second)
    }

    "propagate unexpected errors" in {
      val cached = mkCached(throwingInterpreter, _ => true)

      val ex = intercept[Exception] {
        cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      }
      assert(ex.getMessage === "TEST")
    }

    "propagate business error" in {
      val cached = mkCached(leftReturningInterpreter, _ => true)

      val err = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      assert(err === Left(errors.Error.OneForgeLookupFailed("ERROR")))
    }

    "go to live service after the item expires" in {
      val cached = mkCached(countingInterpreter, _ => false)
      val first  = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      val second = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()

      assert(first.isRight)
      assert(second.isRight)
      assert(first.right.get.price.value !== second.right.get.price.value)
    }

    "goes to live service for disparate keys" in {
      val cached = mkCached(countingInterpreter, _ => true)
      val first  = cached.get(Rate.Pair(AUD, USD)).unsafeRunSync()
      val second = cached.get(Rate.Pair(USD, CHF)).unsafeRunSync()

      assert(first.isRight)
      assert(second.isRight)
      assert(first.right.get.price.value !== second.right.get.price.value)
    }
  }

}

object CachedTest {
  private val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(1))
  private implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ec)

  private implicit val timer: Timer[IO] =
    IO.timer(ec)

  private val refreshPolicy = new RefreshPolicy[IO, Rate.Pair] {
    override def getItemsToRefresh(staleItems: Set[Rate.Pair]): IO[Set[Rate.Pair]] = IO.pure(staleItems)
  }

  private def mkCached(live: Algebra[IO], retentionPolicy: Rate => Boolean): Cached[IO] =
    InMemoryCache[IO, Rate.Pair, Rate]((_, b) => b)
      .flatMap(cache => Cached[IO](live, rate => IO.pure(retentionPolicy(rate)), cache, refreshPolicy))
      .unsafeRunSync()

  private implicit val clock: Clock[IO] = new Clock[IO] {
    override def realTime(unit: TimeUnit): IO[Long]  = IO.pure(0.toLong)
    override def monotonic(unit: TimeUnit): IO[Long] = IO.pure(0.toLong)
  }

  private def countingInterpreter: Algebra[IO] = new Algebra[IO] {
    override protected val F: Functor[IO] = Functor[IO]
    private[this] val counter             = new AtomicInteger(0)

    override def get(pairs: Set[Rate.Pair]): IO[Either[errors.Error, List[Rate]]] =
      Timestamp.now[IO].map(ts => Right(pairs.map(pair => Rate(pair, Price.int(counter.incrementAndGet()), ts)).toList))
  }

  private val throwingInterpreter = new Algebra[IO] {
    override protected val F: Functor[IO] = Functor[IO]

    override def get(pair: Set[Rate.Pair]): IO[Either[errors.Error, List[Rate]]] =
      Sync[IO].delay(throw new Exception("TEST"))
  }

  private val leftReturningInterpreter = new Algebra[IO] {
    override protected val F: Functor[IO] = Functor[IO]

    override def get(pair: Set[Rate.Pair]): IO[Either[errors.Error, List[Rate]]] =
      IO.pure(Left(errors.Error.OneForgeLookupFailed("ERROR")))
  }
}
