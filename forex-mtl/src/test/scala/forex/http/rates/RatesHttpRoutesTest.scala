package forex.http.rates

import java.time.OffsetDateTime

import cats.effect.{ Clock, IO }
import com.typesafe.config.ConfigFactory
import forex.config.ApplicationConfig
import forex.domain.{ Price, Rate, Timestamp }
import forex.http._
import forex.programs.rates.Protocol.GetRatesRequest
import forex.programs.{ rates, RatesProgram }
import io.circe.Json
import org.http4s.{ Method, Request, _ }
import org.scalatest.WordSpec

import scala.concurrent.duration.TimeUnit

class RatesHttpRoutesTest extends WordSpec {
  import RatesHttpRoutesTest._

  "RatesHttpRoutes" should {
    val baseRequest = Request[IO](method = Method.GET, uri = uri"/rates/?from=USD&to=AUD")

    "rethrow exceptions" in {
      val service = mkService(alwaysErrorRatesProgram)
      val response = service.routes(baseRequest).value.unsafeRunSync()
      val apiError = response.get.as[Json].unsafeRunSync().hcursor
      assert(apiError.get[String]("message") === Right("Test error"))
    }

    "route to rates program" in {
      val service = mkService(constantRatesProgram)

      val response = service.routes(baseRequest).value.unsafeRunSync()

      assert(response.isDefined)
      val apiRespnse = response.get.as[Json].unsafeRunSync().hcursor
      assert(apiRespnse.get[String]("from") === Right("USD"))
      assert(apiRespnse.get[String]("to") === Right("AUD"))
      assert(apiRespnse.get[Int]("price") === Right(12))
      assert(apiRespnse.get[OffsetDateTime]("timestamp") === Right(constantTime))
    }

    "not route incorrect query string" in {
      val req     = baseRequest.withUri(uri"/rates/?fro=USD&to=AUD")
      val service = mkService(constantRatesProgram)

      val response = service.routes(req).value.unsafeRunSync()

      assert(response.isEmpty)
    }

    "report error if from Currency is wrong" in {
      val req     = baseRequest.withUri(uri"/rates/?from=USDD&to=AUD")
      val service = mkService(constantRatesProgram)

      val response = service.routes(req).value.unsafeRunSync()

      assert(response.isDefined)
      val json       = response.get.as[Json].unsafeRunSync().hcursor
      val errorField = json.downField("error").downField("invalid_query_params").as[Array[String]]
      assert(errorField.map(_.length) == Right(1))
      assert(errorField.right.get.exists(_.contains("from")))
    }

    "report error if to Currency is wrong" in {
      val req     = baseRequest.withUri(uri"/rates/?from=USD&to=AUDD")
      val service = mkService(constantRatesProgram)

      val response = service.routes(req).value.unsafeRunSync()

      assert(response.isDefined)
      val json       = response.get.as[Json].unsafeRunSync().hcursor
      val errorField = json.downField("error").downField("invalid_query_params").as[Array[String]]
      assert(errorField.map(_.length) == Right(1))
      assert(errorField.right.get.exists(_.contains("to")))
    }

    "report 2 errors if both query params are wrong" in {
      val req     = baseRequest.withUri(uri"/rates/?from=USDD&to=AUDD")
      val service = mkService(constantRatesProgram)

      val response = service.routes(req).value.unsafeRunSync()

      assert(response.isDefined)
      val json       = response.get.as[Json].unsafeRunSync().hcursor
      val errorField = json.downField("error").downField("invalid_query_params").as[Array[String]]
      assert(errorField.map(_.length) == Right(2))
      assert(errorField.right.get.exists(_.contains("from")))
      assert(errorField.right.get.exists(_.contains("to")))
    }
  }
}

object RatesHttpRoutesTest {
  import pureconfig.generic.auto._

  private val config =
    pureconfig.loadConfigOrThrow[ApplicationConfig](ConfigFactory.load("test"), "app")
  private def mkService(program: RatesProgram[IO]) = new RatesHttpRoutes[IO](program, config)

  private implicit val clock: Clock[IO] = new Clock[IO] {
    override def realTime(unit: TimeUnit): IO[Long]  = IO.pure(0.toLong)
    override def monotonic(unit: TimeUnit): IO[Long] = IO.pure(0.toLong)
  }

  private val alwaysErrorRatesProgram = new RatesProgram[IO] {
    override def get(request: GetRatesRequest): IO[Either[rates.errors.Error, Rate]] =
      IO.pure(Left(rates.errors.Error.RateLookupFailed("Test error")))
  }

  private val constantTime = OffsetDateTime.parse("2019-08-08T12:00:00+01:00")
  private val constantRatesProgram = new RatesProgram[IO] {
    override def get(request: GetRatesRequest): IO[Either[rates.errors.Error, Rate]] =
      IO.pure(Right(Rate(Rate.Pair(request.from, request.to), Price.int(12), Timestamp(constantTime))))
  }
}
