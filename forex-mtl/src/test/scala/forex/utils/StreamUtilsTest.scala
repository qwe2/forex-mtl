package forex.utils

import java.util.concurrent.Executors

import cats.effect.{IO, Timer}
import org.scalatest.WordSpec

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class StreamUtilsTest extends WordSpec {
  import StreamUtilsTest._

  "retryUntil" should {
    val delay = 10.millis

    "retry the stream until max attempts is reached" in {
      var x = 0
      val thunk = IO.delay { x = x + 1; x }
      val result = StreamUtils.retryUntil(thunk, (_: Int) > 10, 5, delay, identity)
        .compile.toList.unsafeRunSync()
      assert(result === List(5))
    }

    "finish on success" in {
      var x = 0
      val thunk = IO.delay { x = x + 1; x }
      val result = StreamUtils.retryUntil(thunk, (_: Int) == 3, 5, delay, identity)
        .compile.toList.unsafeRunSync()
      assert(result === List(3))
    }

    "consider initial duration" in {
      val delay = 5.minutes
      var x = 0
      val thunk = IO.delay { x = x + 1; x }
      val result = StreamUtils.retryUntil(thunk, (_: Int) == 3, 5, delay, identity)
        .compile.toList.unsafeRunTimed(500.millis)
      assert(result === None)
    }

    "increment delay" in {
      val delay = 50.millis
      var x = 0
      val thunk = IO.delay { x = x + 1; x }
      val result = StreamUtils.retryUntil(thunk, (_: Int) == 3, 5, delay, _ + 5.minutes)
        .compile.toList.unsafeRunTimed(500.millis)
      assert(result === None)
    }
  }

}

object StreamUtilsTest {
  private val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(1))
  private implicit val timer: Timer[IO] = IO.timer(ec)
}
