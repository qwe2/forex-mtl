import sbt._

object Dependencies {

  object Versions {
    val cats       = "1.6.1"
    val catsEffect = "1.4.0"
    val fs2        = "1.0.4"
    val http4s     = "0.20.9"
    val circe      = "0.11.1"
    val pureConfig = "0.11.1"

    val kindProjector  = "0.10.3"
    val logback        = "1.2.3"
    val scalaCheck     = "1.14.0"
    val scalaTest      = "3.0.8"
    val catsScalaCheck = "0.1.1"
    val shapeless      = "2.3.3"
    val scalaLog       = "3.9.2"
  }

  object Libraries {
    def circe(artifact: String): ModuleID  = "io.circe"   %% artifact % Versions.circe
    def http4s(artifact: String): ModuleID = "org.http4s" %% artifact % Versions.http4s

    lazy val cats       = "org.typelevel" %% "cats-core"      % Versions.cats
    lazy val catsEffect = "org.typelevel" %% "cats-effect"    % Versions.catsEffect
    lazy val alleyCats  = "org.typelevel" %% "alleycats-core" % Versions.cats
    lazy val fs2        = "co.fs2"        %% "fs2-core"       % Versions.fs2

    lazy val http4sDsl       = http4s("http4s-dsl")
    lazy val http4sServer    = http4s("http4s-blaze-server")
    lazy val http4sClient    = http4s("http4s-blaze-client")
    lazy val http4sCirce     = http4s("http4s-circe")
    lazy val circeCore       = circe("circe-core")
    lazy val circeGeneric    = circe("circe-generic")
    lazy val circeGenericExt = circe("circe-generic-extras")
    lazy val circeParser     = circe("circe-parser")
    lazy val circeJava8      = circe("circe-java8")
    lazy val pureConfig      = "com.github.pureconfig" %% "pureconfig" % Versions.pureConfig
    lazy val shapeless       = "com.chuusai" %% "shapeless" % Versions.shapeless

    // Compiler plugins
    lazy val kindProjector = "org.typelevel" %% "kind-projector" % Versions.kindProjector

    // Runtime
    lazy val logback  = "ch.qos.logback"             % "logback-classic" % Versions.logback
    lazy val scalaLog = "com.typesafe.scala-logging" %% "scala-logging"  % Versions.scalaLog

    // Test
    lazy val scalaTest      = "org.scalatest"     %% "scalatest"       % Versions.scalaTest
    lazy val scalaCheck     = "org.scalacheck"    %% "scalacheck"      % Versions.scalaCheck
    lazy val catsScalaCheck = "io.chrisdavenport" %% "cats-scalacheck" % Versions.catsScalaCheck
  }

}
