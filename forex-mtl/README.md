# Running the project

* Tested on openjdk-8. There still seems to be some issue with http-blaze on openjdk-11 which is causing
https://github.com/http4s/blaze/issues/258, so should be run on JDK 1.8 for now.
* Run with `sbt -J-Doneforge.api.key=$ONEFORGE_API_KEY run` where `$ONEFORGE_API_KEY` is your API key from https://1forge.com/account/my-services.
* Generate coverage report: `sbt clean coverage test it:test coverageReport`.

# Considerations regarding the project

> An internal user of the application should be able to ask for an exchange rate between 2 given currencies, 
> and get back a rate that is not older than 5 minutes. The application should at least support 10.000 
> requests per day.

The basic 1Forge API tier supports 5000 requests a day. 
If we put more than one currency pair (or even all of them) in the price request, that
still counts as one request towards the quota.

My solution caches all the possible rates that one can query. When a request comes in, the price is 
retrieved from the in-memory cache and returned as response if it exists and isn't older than 5 minutes.
If it's not in the cache, or is too old, the application queries the live API for all of the currency pairs
in one request, and the result is put back into the cache. With this approach, we do 288 live requests
at most.

This works well because only a subset of 1Forge's supported currency pairs are defined in the project.
If we need more pairs in the future, it might be needed to split up the live requests into multiple ones,
or to not cache everything (ie. only cache the top `n` most requested pairs). That said, `forex.1forge.com/1.0.3/symbols`
returns 701 pairs currently. These should still fit into one `GET` request and wouldn't use too much memory.

The cache retention times comes from `application.conf` and it could be reduced to a lower number.
It could go as low as `17.28s` to potentially exhaust all the requests we can use. An improvement could
be to read the API quota on startup and derive the cache eviction time from that, or even dynamically 
adjust it during the day depending on request rate.

Cache refresh is synchronized. There can only be one request refreshing the cache with live values. The
other requests will optimistically try to acquire the lock and do their refresh. If they don't get the lock,
they will retry reading from the cache, and if the item is not still not there, they will try refreshing again.
This process is repeated until the item gets into the cache or the retries time out after about 1.2 seconds.
This is based on the assumption that the concurrent refresh will already fetch the the item for the other
requests as well. This is true in 1Froge's case, but might not be in future cases. A more sophisticated
approach would be to maintain a list of pairs that are being refreshed by the concurrent request, and if
what the other request needs is on the list, it would wait for the concurrent refresh to finish 
and then read the cache again. If it's not on the list, it would kick off a concurrent cache refresh with
the difference of the already-refreshing items and the to-be-refreshed items. 

Refreshing the cache on a request means that those requests that need to go to the live service have 
a higher latency than those that do not. The latency could probably be flattened if required, by 
doing the live refresh periodically on a concurrent stream.

To accomodate querying multiple pairs from the live service, the interface was changed to take a set of 
currency pairs and return a list of rates.

